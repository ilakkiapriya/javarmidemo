package com.demo.javarmi.server;

import com.demo.javarmi.api.Adder;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class ServerAdder extends UnicastRemoteObject implements Adder {

    protected ServerAdder() throws RemoteException {
        super();
    }

    @Override
    public int add(int a, int b) throws RemoteException {
        return a+b;
    }
}
