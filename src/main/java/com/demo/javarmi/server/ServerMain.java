package com.demo.javarmi.server;

import com.demo.javarmi.api.Adder;

import java.rmi.Naming;

public class ServerMain {
    public static void main(String args[]){
        try{
            Adder stub=new ServerAdder();
            Naming.rebind("rmi://localhost:5000/demormi",stub);
        }catch(Exception e){System.out.println(e);}
    }

}
